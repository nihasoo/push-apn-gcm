window.onload = function () {

    'use strict';

    var Cropper = window.Cropper;
    var console = window.console || {
            log: function () {
            }
        };
    var container = document.querySelector('.img-container');
    var image = container.getElementsByTagName('img').item(0);
    var download = document.getElementById('download');
    var actions = document.getElementById('actions');
    var dataX = document.getElementById('dataX');
    var dataY = document.getElementById('dataY');
    var dataHeight = document.getElementById('dataHeight');
    var dataWidth = document.getElementById('dataWidth');
    var dataRotate = document.getElementById('dataRotate');
    var dataScaleX = document.getElementById('dataScaleX');
    var dataScaleY = document.getElementById('dataScaleY');
    var options = {
        aspectRatio: 1,
        preview: '.img-preview',
        autoCropArea: 1,
        restore: false,
        guides: false,
        center: false,
        highlight: false,
        cropBoxMovable: false,
        cropBoxResizable: false,
        build: function () {
            //console.log('build');
        },
        built: function () {
            //console.log('built');
        },
        cropstart: function (data) {
            console.log('cropstart', data.action);
        },
        cropmove: function (data) {
            console.log('cropmove', data.action);
        },
        cropend: function (data) {
            console.log('cropend', data.action);
        },
        crop: function (data) {
            dataX.value = Math.round(data.x);
            dataY.value = Math.round(data.y);
            dataHeight.value = 500;
            dataWidth.value = 500;
            dataRotate.value = !isUndefined(data.rotate) ? data.rotate : '';

        },
        zoom: function (data) {
            console.log('zoom', data.ratio);
        }
    };
    //var

    var cropper = new Cropper(image, {
        dragMode: 'move',
        aspectRatio: 1,
        //autoCropArea: 0.20,
        // defaults for fix crop area
        restore: false,
        guides: false,
        center: false,
        highlight: false,
        cropBoxMovable: false,
        cropBoxResizable: false,
    });

    window.cropper = cropper;


    function isUndefined(obj) {
        return typeof obj === 'undefined';
    }


    function preventDefault(e) {
        if (e) {
            if (e.preventDefault) {
                e.preventDefault();
            } else {
                e.returnValue = false;
            }
        }
    }

    // Buttons
    if (!document.createElement('canvas').getContext) {
        $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
    }

    if (typeof document.createElement('cropper').style.transition === 'undefined') {
        $('button[data-method="rotate"]').prop('disabled', true);
        $('button[data-method="scale"]').prop('disabled', true);
    }

    // Methods
    actions.querySelector('.docs-buttons').onclick = function (event) {
        var e = event || window.event;
        var target = e.target || e.srcElement;
        var result;
        var input;
        var data;

        if (!cropper) {
            return;
        }

        while (target !== this) {
            if (target.getAttribute('data-method')) {
                break;
            }

            target = target.parentNode;
        }

        if (target === this || target.disabled || target.className.indexOf('disabled') > -1) {
            return;
        }

        data = {
            method: target.getAttribute('data-method'),
            target: target.getAttribute('data-target'),
            option: target.getAttribute('data-option'),
            secondOption: target.getAttribute('data-second-option')
        };
        //alert(data.option);


        if (data.method) {
            if (typeof data.target !== 'undefined') {
                input = document.querySelector(data.target);

                if (!target.hasAttribute('data-option') && data.target && input) {
                    try {
                        data.option = JSON.parse(input.value);
                    } catch (e) {
                        console.log(e.message);
                    }
                }
            }

            if (data.method === 'getCroppedCanvas') {
                data.option = JSON.parse(data.option);
            }

            result = cropper[data.method](data.option, data.secondOption);


            switch (data.method) {
                case 'scaleX':
                case 'scaleY':
                    target.setAttribute('data-option', -data.option);
                    break;

                case 'getCroppedCanvas':
                    if (result) {




                        // Bootstrap's Modal
                        $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

                        if (!download.disabled) {
                            download.href = result.toDataURL();
                        }
                    }

                    break;

                case 'destroy':
                    cropper = null;
                    break;
            }

            if (typeof result === 'object' && result !== cropper && input) {
                try {
                    input.value = JSON.stringify(result);
                } catch (e) {
                    console.log(e.message);
                }
            }

        }
    };

    document.body.onkeydown = function (event) {
        var e = event || window.event;

        if (!cropper || this.scrollTop > 300) {
            return;
        }

        switch (e.keyCode) {
            case 37:
                preventDefault(e);
                cropper.move(-1, 0);
                break;

            case 38:
                preventDefault(e);
                cropper.move(0, -1);
                break;

            case 39:
                preventDefault(e);
                cropper.move(1, 0);
                break;

            case 40:
                preventDefault(e);
                cropper.move(0, 1);
                break;
        }
    };

    // event for resizing image and and form subit with
    $('#resize').click(function(e){
        e.preventDefault();
        // confirmation for resizing image
        if(confirm('Are you sure ?')){
            // set converted binary string to the field base-img-str
            $("#base-image-str").val(window.cropper.getCroppedCanvas().toDataURL());
            // form submit
            setTimeout(function(){
                $("#image-resize-form").submit();
            }, 2000);

        }
    });

};
