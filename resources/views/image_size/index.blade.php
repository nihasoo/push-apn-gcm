<table>
    <thead>
    <tr>
        <th>Name </th>
        <th>Type </th>
        <th>Height</th>
        <th>Width</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($image_sizes as $image_size)
        <tr>

            <td>{{$image_size->name}}</td>
            <td>{{$image_size->type}}</td>
            <td>{{$image_size->height}}</td>
            <td>{{$image_size->width}}</td>
            <td><a href="{{ route('image-size.edit' , $image_size->id) }}"></a>
                <a href="{{ route('image-size.destroy' , $image_size->id) }}" class="btn btn-danger action_confirm" data-method="delete" data-token="{{ csrf_token() }}"><i class="glyphicon glyphicon-remove"></i></a></td>
        </tr>
    @endforeach
    </tbody>
</table>