
{!! Form::model($model, array('method' => 'put',  'route' => array('image-size.update' , $model->id), 'class' => 'form-horizontal')) !!}
@include('image_size.form')
{!! Form::close() !!}