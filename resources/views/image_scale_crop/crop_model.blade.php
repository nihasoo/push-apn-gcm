<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="A basic demo of Cropper.">
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, image cropping, cropper, cropperjs, cropper.js, front-end, web development">
    <meta name="author" content="Fengyuan Chen">
    <title>Cropper.js</title>
    <link rel="stylesheet" href="/<?php print drupal_get_path('module','brussels_life_image')?>/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/<?php print drupal_get_path('module','brussels_life_image')?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/<?php print drupal_get_path('module','brussels_life_image')?>/assets/css/cropper.css">
    <link rel="stylesheet" href="/<?php print drupal_get_path('module','brussels_life_image')?>/assets/css/main.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->




</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header -->



<!-- Content -->
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <!-- <h3 class="page-header">Demo:</h3> -->
            <div class="img-container">
                <img src="<?php print $base_image_uri; ?>" alt="Picture">
            </div>
        </div>
        <div class="col-md-3">
            <!-- <h3 class="page-header">Preview:</h3> -->
            <div class="docs-preview clearfix">
                <div class="img-preview preview-lg"></div>
            </div>

            <!-- <h3 class="page-header">Data:</h3> -->
            <div class="docs-data">
                <div class="input-group input-group-sm">
                    <label class="input-group-addon" for="dataX">X</label>
                    <input type="text" class="form-control" id="dataX" placeholder="x">
                    <span class="input-group-addon">px</span>
                </div>
                <div class="input-group input-group-sm">
                    <label class="input-group-addon" for="dataY">Y</label>
                    <input type="text" class="form-control" id="dataY" placeholder="y">
                    <span class="input-group-addon">px</span>
                </div>
                <div class="input-group input-group-sm">
                    <label class="input-group-addon" for="dataWidth">Width</label>
                    <input type="text" value="<?php print $gallery_image_width?>" class="form-control" id="dataWidth" placeholder="width">
                    <span class="input-group-addon">px</span>
                </div>
                <div class="input-group input-group-sm">
                    <label class="input-group-addon" for="dataHeight">Height</label>
                    <input type="text" value="<?php print $gallery_image_height?>" class="form-control" id="dataHeight" placeholder="height">
                    <span class="input-group-addon">px</span>
                </div>
                <div class="input-group input-group-sm">
                    <label class="input-group-addon" for="dataRotate">Rotate</label>
                    <input type="text" class="form-control" id="dataRotate" placeholder="rotate">
                    <span class="input-group-addon">deg</span>
                </div>


            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group">
                        <form action="/gallery-image-creator/<?php print $fid; ?>" id="image-resize-form" method="post">

                            <textarea style="display: none;" name="base_string" id="base-image-str" cols="30"
                                      rows="10"></textarea>
                            <input type="hidden" name="image_to_replace" value="<?php  print  $image_to_replace ?> ">
                            <input type="hidden"  id="file_replace_status" value="<?php print $file_replace_status?>"/>

                            <button type="submit" class="btn btn-primary" id="resize">Resize</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="actions">
        <div class="col-md-9 docs-buttons">
            <!-- <h3 class="page-header">Toolbar:</h3> -->
            <div class="btn-group">
                <button type="button" class="btn btn-primary" data-method="setDragMode" data-option="move" title="Move">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setDragMode(&quot;move&quot;)">
              <span class="fa fa-arrows"></span>
            </span>
                </button>
                <button type="button" class="btn btn-primary" data-method="setDragMode" data-option="crop" title="Crop">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setDragMode(&quot;crop&quot;)">
              <span class="fa fa-crop"></span>
            </span>
                </button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(0.1)">
              <span class="fa fa-search-plus"></span>
            </span>
                </button>
                <button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(-0.1)">
              <span class="fa fa-search-minus"></span>
            </span>
                </button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(-10, 0)">
              <span class="fa fa-arrow-left"></span>
            </span>
                </button>
                <button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(10, 0)">
              <span class="fa fa-arrow-right"></span>
            </span>
                </button>
                <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, -10)">
              <span class="fa fa-arrow-up"></span>
            </span>
                </button>
                <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, 10)">
              <span class="fa fa-arrow-down"></span>
            </span>
                </button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(-45)">
              <span class="fa fa-rotate-left"></span>
            </span>
                </button>
                <button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(45)">
              <span class="fa fa-rotate-right"></span>
            </span>
                </button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-primary" data-flip="horizontal" data-method="scaleX" data-option="-1" title="Flip Horizontal">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.scaleX(-1)">
              <span class="fa fa-arrows-h"></span>
            </span>
                </button>
                <button type="button" class="btn btn-primary" data-flip="vertical" data-method="scaleY" data-option="-1" title="Flip Vertical">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.scaleY(-1)">
              <span class="fa fa-arrows-v"></span>
            </span>
                </button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-primary" data-method="crop" title="Crop">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.crop()">
              <span class="fa fa-check"></span>
            </span>
                </button>
                <button type="button" class="btn btn-primary" data-method="clear" title="Clear">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.clear()">
              <span class="fa fa-remove"></span>
            </span>
                </button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-primary" data-method="disable" title="Disable">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.disable()">
              <span class="fa fa-lock"></span>
            </span>
                </button>
                <button type="button" class="btn btn-primary" data-method="enable" title="Enable">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.enable()">
              <span class="fa fa-unlock"></span>
            </span>
                </button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-primary" data-method="reset" title="Reset">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.reset()">
              <span class="fa fa-refresh"></span>
            </span>
                </button>
                <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                    <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
            <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
              <span class="fa fa-upload"></span>
            </span>
                </label>
                <button type="button" class="btn btn-primary" data-method="destroy" title="Destroy">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.destroy()">
              <span class="fa fa-power-off"></span>
            </span>
                </button>
            </div>


        </div><!-- /.docs-buttons -->

    </div>

</div>



<!-- Scripts -->
<script src="/<?php print drupal_get_path('module','brussels_life_image')?>/assets/js/jquery.min.js"></script>
<script src="/<?php print drupal_get_path('module','brussels_life_image')?>/assets/js/bootstrap.min.js"></script>
<script src="/<?php print drupal_get_path('module','brussels_life_image')?>/assets/js/cropper.js"></script>
<script src="/<?php print drupal_get_path('module','brussels_life_image')?>/assets/js/main.js"></script>

<script>
    (function($){
        var file_replace_status   = $('#file_replace_status').val();

        if(file_replace_status == 1) {


        }
    })(jQuery)

</script>
</body>
</html>




























<?php /**



<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link  href="<?php print drupal_get_path('module','membercount')?>/css/cropper.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?php print drupal_get_path('module','membercount')?>/js/cropper.js"></script>


    <script>
        (function ($) {



            document.addEventListener('DOMContentLoaded', function () {
                var image = document.getElementById('image');
                var button = document.getElementById('button');
                var result = document.getElementById('result');
                var croppable = false;
                var cropper = new Cropper(image, {
                    aspectRatio: 1,
                    viewMode: 1,
                    autoCrop: 1,
                    built: function () {
                        croppable = true;
                    }
                });

                button.onclick = function () {
                    var croppedCanvas;

                    var croppedImage;

                    if (!croppable) {
                        return;
                    }

                    // Crop
                    croppedCanvas = cropper.getCroppedCanvas();



                    // Show
                    croppedImage = document.createElement('img');
                    croppedImage.src = croppedCanvas.toDataURL()
//                alert(croppedImage.src );


                    result.innerHTML = '';
                    result.appendChild(croppedImage);


                    $.ajax({
                        type: "POST",
                        url : Drupal.settings.basePath+'temp_cropped_file_upload',
                        data: { cropped_file: croppedImage.src, fid_before_crop:$("#fid").val()
                        },
                        success:function(data){
                            alert(data);
//                        window.opener.$("#cropped_temp_img_id").val(data);
                            window.close();
                        }




                    });


                };

            });

        })(jQuery);
    </script>


</head>
<body>
<div class="container">
    <h3>Image</h3>
    <div>
        <input type="hidden" id="fid" value="<?php print $fid?>">
        <img id="image" src="<?php print variable_get('file_public_path', conf_path() . '/files') . "/".$base_image_uri?>" alt="Picture">
    </div>
    <h3>Result</h3>
    <p>
        <button type="button" id="button">Crop</button>
    </p>
    <div id="result"></div>
</div>
</body>
</html>


*/?>

