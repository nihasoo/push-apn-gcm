<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.js"></script>

<script src="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/js/Jcrop.js"></script>
<link rel="stylesheet" href="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/css/Jcrop.css" type="text/css">


<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.9.3/vue-resource.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.1.0/vue-strap.min.js" type="text/javascript"></script>

{!! Form::model($model, array('route' => array('image-scale-crop.store'), 'class' => 'form-horizontal', 'id'=>'image-uploader','files' => TRUE)) !!}
@include('image_scale_crop.form')
{!! Form::close() !!}


<!--<script type="text/javascript">
    (function ($, window, document) {
        // Most basic attachment example



        $('#crop_image').click(function () {
            var file = $("#uploadeImage").get(0).files[0];


            if (file) {
                var reader = new FileReader();

                reader.onload = function (readerEvt) {
                    var binaryString = readerEvt.target.result;
                    var data = {'data': (btoa(binaryString))};
                    var image_crop_window = window.open("/image-scale-crop/crop?image_data=" + data, 'crop window', 'height=400,width=400');
                    if (window.focus) {
                        image_crop_window.focus()
                    }
                    return false;

                };
                reader.readAsBinaryString(file);

            }


        });

    })(jQuery, window, document);
</script>


