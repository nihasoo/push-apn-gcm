<section id="image-resizer">
    {!! Form::file('image' , ['id'=>'uploadeImage', 'v-model' => 'image_file', '@change' => 'validateFile']) !!}
    <div id="interface" v-show="base_img"><img v-bind:src="base_img" id="target"/></div>
    <div v-show="base_img" class="row">
        <div class="col-lg-3">
            X
            <div class="large-8 medium-8 small-8 columns"><input v-bind:value='cropped_obj_x' type="text" name="cordinates[x]"
                                                                 id="crop-x"/></div>
        </div>
        <div class="col-lg-3">

            Y
            <div class="large-8 medium-8 small-8 columns"><input v-bind:value='cropped_obj_y' type="text" name="cordinates[y]"
                                                                 id="crop-y"/></div>
        </div>
        <div class="col-lg-3">
            W
            <div class="large-8 medium-8 small-8 columns"><input v-bind:value='cropped_obj_w' type="text" name="cordinates[w]"
                                                                 id="crop-w"/></div>
        </div>
        <div class="col-lg-3">

            H
            <div class="large-8 medium-8 small-8 columns"><input v-bind:value='cropped_obj_h' type="text" name="cordinates[h]"
                                                                 id="crop-h"/></div>
        </div>
    </div>
    <div v-show="base_img" class="col-md-3">

        <button type="button" @click="processImage">Upload & Process</button>
    </div>
    <div v-for="(size,image) in images">
        <input type='checkbox' name='sizes[]' v-bind:value="image.size_id"/>
        <img v-bind:src="image.binary_image.encoded" alt="">
    </div>
    {!! Form::submit('Save', ['id'=>'upload','class' => 'btn btn-primary' ,'v-show'=>"base_img"]) !!}

</section>


{{--@{{ images | json }}--}}

<script type="text/javascript">
    var ImageUploader = new Vue({
        el: '#image-resizer',
        data: {
            image_file: null,
            image_obj: null,
            base_img: null,
            cropped_obj_w: null,
            cropped_obj_h: null,
            cropped_obj_x: null,
            cropped_obj_y: null,
            images: [],
            base_img_binary: null,
            file_types:['image/gif','image/png','image/jpg','image/jpeg'],
        },
        methods: {
            // uploading  base image
            validateFile: function (file) {
                //get uploaded image object
                this.base_img = file.target.files[0];
                var reader = new FileReader(), that = this;
                reader.onload = function (readerEvt) {
                    // binary string of the base image
                    var binaryString = readerEvt.target.result;
                    that.image_obj = file.target.files[0];
                    // bind the binary string
                    that.base_img_binary =  btoa(binaryString);
                    //set the base image binary string to the base image source to display
                    that.$set('base_img', 'data:image/jpeg;base64,' + btoa(binaryString));


                };
                reader.readAsBinaryString(this.base_img);
            },

            processImage: function () {

                // validate the file type
                if(this.file_types.indexOf(this.image_obj.type) > -1){
                    // Validate name for DOM hacks
                if (this.image_file == this.image_obj.name) {
                    // http post request for resizing the cropped base image
                        this.$http.post('/image-scale-crop/preview',
                                // Data
                                {
                                    // binary string of the base image
                                    'data': this.base_img_binary,
                                    //cropped image cordinates
                                    'cordinates': {
                                        'w': this.cropped_obj_w,
                                        'h': this.cropped_obj_h,
                                        'x': this.cropped_obj_x,
                                        'y': this.cropped_obj_y,
                                    },
                                },
                                // Options
                                {
                                    // setting csrf token
                                    headers: {
                                        'X-CSRF-Token': '{{ csrf_token() }}'
                                    }
                                }).then(function (response) {
                            // validate
                            //if the status is true
                            if (response.json().status) {
                                // bind different sized images of the cropped base image
                                this.$set('images', response.json().data);
                            }
                        });
                }
            }else{
                    alert("invalid file type");
                    return false;
                }
            }
        },
        ready: function () {
            $('#target').Jcrop({
                //default crop area
                setSelect: [175, 100, 400, 300]
            });
            var obj = this;


            $('#interface').on('cropmove cropend', function (e, s, c) {

                // binding  cropped image cordinates to cordinat fields
                obj.cropped_obj_w = c.w;
                obj.cropped_obj_h = c.h;
                obj.cropped_obj_x = c.x;
                obj.cropped_obj_y = c.y;
            });


        }
    });
</script>