<a href="{{ route('news-letter.export-csv' ) }}">Export CSV</a>
<table>
    <thead>
    <tr>
        <th>Email</th>
        <th>Birthday</th>
        <th>Postal Code</th>
    </tr>
    </thead>
    <tbody>
    @if(count($model))
    @foreach($model as $row)
        <tr>

            <td>{{$row->email}}</td>
            <td>{{$row->birthday}}</td>
            <td>{{$row->postal_code}}</td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>