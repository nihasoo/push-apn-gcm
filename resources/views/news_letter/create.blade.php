<section id="news_letter_subscription">
    <p style="color: green">@{{success_message}}</p>
    <span style="color: red">@{{fail_message}}</span>
    <form v-on:submit.prevent="submitForm">
        <div class="formElement">
            <input type="text" v-model="email" placeholder="Your Email" name="email">
        </div>
        <div>

            {{--<select  name="birthday_month">--}}
            {{--<option selected="" value=""></option>--}}
            {{--@foreach($months as $month)--}}
            {{--<option value="{{$month}}">{{$month}}</option>--}}
            {{--@endforeach--}}

            {{--</select>--}}

            {{--<select  name="birthday_date">--}}
            {{--<option selected="" value=""></option>--}}
            {{--@foreach($dates as $date)--}}
            {{--<option value="{{$date}}">{{$date}}</option>--}}
            {{--@endforeach--}}
            {{--</select>--}}

            {{--<select  name="birthday_year">--}}
            {{--<option selected="" value=""></option>--}}
            {{--@foreach($years as $year)--}}
            {{--<option value="{{$year}}">{{$year}}</option>--}}
            {{--@endforeach--}}
            {{--</select>--}}

            <input type="text" v-model="dob" placeholder="DOB" name="birthday">
            <p v-show="dob_error" style="color: red">Invalid Date Format</p>
            <div class="clearfix"></div>
        </div>
        <div>
            <input type="text" v-model="postal_code" placeholder="Postal code" name="postal_code">
        </div>
        <div>
            <div>
                <label for="">FR</label>
                <input type="radio" value="fr" v-model="language" name="language">
            </div>
            <div>
                <label for="">EN</label>
                <input type="radio" value="en" v-model="language" name="language">
            </div>
            <div>
                <label for="">NL</label>
                <input type="radio" value="nl" v-model="language" name="language">
            </div>
        </div>
        <div>
            <button>
                <span class="icon icon-mail"></span>
                &nbsp; Submit
            </button>
        </div>
        @{{language}}

    </form>
</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.9.3/vue-resource.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.1.0/vue-strap.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script type="text/javascript">

    var NewsLetterForm = new Vue({

        el: '#news_letter_subscription',
        data: {
            email: null,
//            birth_date: null,
//            birth_year: null,
//            birth_month: null,
            postal_code: null,
            language: null,
            dob: null,
            dob_error: false,
            success_message: null,
            fail_message: null,

        },

        methods: {
            submitForm: function () {

                // validate the date format
                if (moment(this.dob).format('YYYY-MM-DD') == 'Invalid date') {
                    this.dob_error = true;
                    return false;
                }
                this.dob_error = false;
                // http post request
                this.$http.post('/news-letter/add',
                        // Data
                        {
                            // form data
                            'data': {
                                'email': this.email,
                                'birthday': moment(this.dob).format('YYYY-MM-DD'),
                                'language': this.language,
                                'postal_code': this.postal_code
                            }

                        },
                        // Options
                        {
                            // setting csrf token
                            headers: {
                                'X-CSRF-Token': '{{ csrf_token() }}'
                            }
                        }).then(function (response) {
                    //if the status is true
                    if (response.json().status) {
                        this.success_message = "successfully added"
                    } else {
                        this.fail_message = "failed";
                    }
                });
            },
            computed: {
                birth_full_date: {
                    get: function () {
                        return this.birth_date + '-' + this.birth_month + '-' + this.birth_year
                    },
                    set: function () {

                    }
                }
            }

        },
        ready: function () {
        }


    })


</script>