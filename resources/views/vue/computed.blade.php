<section id="computed_prop">

  <h1>  Score @{{ skill }}</h1>

    <input type="text" v-model="points"/>
    <input type="text" v-model="first"/>
    <input type="text" v-model="last"/>
    <h1>@{{ fullname }}</h1>

</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.4/vue.js" type="text/javascript"></script>

<script type="text/javascript">


    var app = new Vue({

        el: '#computed_prop',
        data: {
            points: 90,
            first:'',
            last:'',
//            fullname: ''
        },
        methods: {},
        // computed properties are reactive
        // watch on an outcome
        computed: {
            skill: function () {
                if ((this.points) < 100) {
                    return 'intermediate'
                }
                return 'superior';
            },
            fullname: function (){
                return this.first+' '+this.last;
            }
        },
        // watch on seperate properties
//        watch : {
//            first: function(first){
//                return this.fullname = first+' ' +this.last;
//            },
//            last: function(last){
//                return this.fullname =  this.first+' ' +last;
//            }
//        }

    })

</script>