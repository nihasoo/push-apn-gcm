<style type="text/css">
    .completed{
       text-decoration: underline;
    }
</style>

<section id="list-rendering">

    <pretty heading="up" :list="terms">


    </pretty>
    <pretty heading="down">


    </pretty>




</section>
<template id="list-template">

    @{{ heading }}:
    <ul >
        <li v-for="term in list" :class="{ 'completed': term.completed }"> @{{ term.body }}</li>
    </ul>
</template>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js" type="text/javascript"></script>

<script type="text/javascript">

    Vue.component('pretty',{
        template:'#list-template',
        props: ['heading','list']
    })
    var app = new Vue({
        el:'#list-rendering',
        data:{
            showClass:'complete',
            terms : [
                {body:'go',completed:false},
                {body:'hide',completed:false},
                {body:'jump',completed:true}
            ]

        }


    })

</script>