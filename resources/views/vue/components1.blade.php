<section id = "app">
{{--<button @click="count +=1">count @{{count}}</button>--}}
{{--<button @click="counter">mincount @{{count}}</button>--}}

    <counter heading="Likes"></counter>
    <counter heading="Dislikes"></counter>

</section>

<section id = "app-prime">
    {{--<button @click="count +=1">count @{{count}}</button>--}}
    {{--<button @click="counter">mincount @{{count}}</button>--}}

    {{--<counter heading="Likes"></counter>--}}

</section>
<template id="counter-template">
    <h1>@{{heading}}</h1>
    <button @click = " count +=1 " > counter @{{count}} </button>

</template>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js" type="text/javascript"></script>
<script type="text/javascript">

    //global component as it can be used in both app element and app-prime elements - data are defined inside the component since it's in global scope - the component
//    Vue.component('counter',{
//        template:'#counter-template',
//        props:['heading'],
//        data : function(){
//            return {count: 0}
//        }
//    });

    // root vue instance

    var Base  = new Vue({
        el: '#app',
        data:{
            count:0
        },
        methods:{
                counter  : function(){
                    this.count +=1;
                }
        },
        // local scope of components
        components:{
            counter:{
                template:'#counter-template',
                props:['heading'],
                data : function(){
                    return {count: 0}
                }
            }
        }

    });
    var prime  = new Vue({
        el: '#app-prime',
    })

</script>prod.BlifeV5.local