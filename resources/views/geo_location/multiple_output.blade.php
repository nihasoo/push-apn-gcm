<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>$.geocomplete()</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}"/>
    <style type="text/css" media="screen">
        form {
            width: 300px;
            float: left;
            margin-left: 20px
        }

        fieldset {
            width: 320px;
            margin-top: 20px
        }

        fieldset strong {
            display: block;
            margin: 0.5em 0 0em;
        }

        fieldset input {
            width: 95%;
        }

        ul span {
            color: #999;
        }
    </style>
</head>
<body>
<section id="gmap">


    <form>
        <input id="geocomplete" v-bind:value="geocomplete" type="text" placeholder="Type in an address" size="90"/>
        <input id="find" type="button" value="find"/>
    </form>
    <pre id="logger">Log:</pre>
</section>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCwqwLajSpjVeMoMVF9zivX7PDZfu9a9y4&sensor=false&amp;libraries=places"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script src="{{ URL::asset('js/jquery.geocomplete.js') }}"></script>
<script src="{{ URL::asset('js/logger.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.9.3/vue-resource.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.1.0/vue-strap.min.js" type="text/javascript"></script>
<script>
    var geoLocation = new Vue({
        el: '#gmap',
        data: {
            addresses: [],
            geocomplete: ""
        },
        methods: {},

        ready: function () {


            this.$http.get('/geo-location/addresses').then(function (response) {

                if (response.json().status) {
                    var that = this;
                    $.each(response.json().addresses, function (key, value) {
                        that.geocomplete = value;

                        Vue.nextTick(function () {
                            $("#geocomplete").geocomplete()
                                    .bind("geocode:result", function (event, result) {
                                        $.log("Result: " + JSON.stringify(result));
                                    })

                            $("#find").click(function () {
                                $("#geocomplete").trigger("geocode");
                            }).click();


                        })


                    });


                }
            });
        }


    })
</script>

</body>
</html>