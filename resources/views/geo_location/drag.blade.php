<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html" ng-app="myapp">
<head>
    <title>Geo Localization</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}"/>
    <style type="text/css" media="screen">
        form {
            width: 300px;
            float: left;
            margin-left: 20px
        }

        fieldset {
            width: 320px;
            margin-top: 20px
        }

        fieldset strong {
            display: block;
            margin: 0.5em 0 0em;
        }

        fieldset input {
            width: 95%;
        }

        ul span {
            color: #999;
        }
    </style>
</head>
<body>
<section id="gmap">
    <div class="map_canvas"></div>

    <form>
        <input id="geocomplete_trigger" name="geocomplete" type="text" v-bind:value="geocomplete_trigger"/>
        <input id="find" v-on="click: triggerGeoLocation" type="button" value="find"/>
        <fieldset>
            <label>Latitude</label>
            <input name="lat" type="text" v-bind:value="lat">

            <label>Longitude</label>
            <input name="lng" type="text" v-bind:value="lng">

            <label>Formatted Address</label>
            <input name="formatted_address" type="text" v-bind:value="geocomplete_trigger">
        </fieldset>

    </form>
</section>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCwqwLajSpjVeMoMVF9zivX7PDZfu9a9y4&sensor=false&amp;libraries=places"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script src="{{ URL::asset('js/jquery.geocomplete.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.9.3/vue-resource.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.1.0/vue-strap.min.js" type="text/javascript"></script>
<script>

    var geoLocation = new Vue({
        el: '#gmap',
        data: {
            addresses: [],
            geocomplete_trigger: null,
            lat : null,
            lng : null
        },
        methods: {
            triggerGeoLocation : function (){
                $("#geocomplete_trigger").trigger("geocode");
            }
        },

        ready: function () {

            $("#geocomplete_trigger").geocomplete({
                map: ".map_canvas",
                details: "form ",
                markerOptions: {
                    draggable: true
                }
            });

            this.$http.get('/geo-location/addresses').then(function (response) {

                if (response.json().status) {
                    var that = this;
                    $.each(response.json().addresses, function (key, value) {
                        that.geocomplete_trigger = value;
                        Vue.nextTick(function () {
                            that.triggerGeoLocation();
                        $("#geocomplete_trigger").bind("geocode:dragged", function (event, latLng) {
                            that.lat  = latLng.lat();
                            that.lng  = latLng.lng();

                        });
                        });
                    });

                }
            });
        }


    })
</script>

</body>
</html>