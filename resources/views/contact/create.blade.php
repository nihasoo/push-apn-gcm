<section id="contact_form">
    <p style="color: green">@{{success_message}}</p>
    <span style="color: red">@{{fail_message}}</span>
    <form v-on:submit.prevent="submitForm">
        <div class="formElement">
            <input type="text" v-model="name" placeholder="Your Name" name="name">
        </div>
        <div class="formElement">
            <input type="text" v-model="email" placeholder="Your Email" name="email">
        </div>

            <div class="formElement">
            <textarea v-model="message" placeholder="Feel free to enter your details (telephone, fax ...) to facilitate contact with the seller" name="message"></textarea>
        </div>

        <div>
            <button>
                <span class="icon icon-mail"></span>
                &nbsp; Submit
            </button>
        </div>
    </form>
</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.9.3/vue-resource.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.1.0/vue-strap.min.js" type="text/javascript"></script>

<script type="text/javascript">

    var ContactForm = new Vue({

        el: '#contact_form',
        data: {
            name: null,
            email: null,
            message:null,
            success_message: null,
            fail_message: null,

        },

        methods: {
            submitForm: function () {

                this.$http.post('/contact/add',
                        // Data
                        {
                            // form data
                            'data': {
                                'name': this.name,
                                'email': this.email,
                                'message': this.message,

                            }

                        },
                        // Options
                        {
                            // setting csrf token
                            headers: {
                                'X-CSRF-Token': '{{ csrf_token() }}'
                            }
                        }).then(function (response) {
                    //if the status is true
                    if (response.json().status) {
                        this.success_message = "successfully added"
                    } else {
                        this.fail_message = "failed";
                    }
                });
            },
        },
        ready: function () {
        }


    })


</script>