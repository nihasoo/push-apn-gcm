<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxonomyVocabulary extends Model
{
  protected $fillable = [
    'vid',
    'name', // English name
    'slug', // Same as the English name in snake case or -
    'en_title',
    'fr_title',
    'nl_title',
    'en_description',
    'fr_description',
    'nl_description',
  ];

  public function Taxonomy(){
    return $this->hasMany('App\TaxonomyTerm');
  }

}
