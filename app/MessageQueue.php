<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageQueue extends Model
{
  protected $fillable = ['user_identification_id'];

  public function user_identification(){
    return $this->belongsTo('App\UserIdentification');
  }
}
