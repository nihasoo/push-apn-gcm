<?php
  namespace App\Libraries;
  use Intervention\Image\Facades\Image;
  class Helpers{

    public static function customHelper(){
      $img = Image::make(public_path().'/foo.jpg')->fit(200, 200, function ($constraint) {
        $constraint->upsize();
      },'left');
      return ($img->encode('data-url'));
    }
  }