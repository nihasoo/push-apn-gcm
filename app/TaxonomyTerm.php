<?php

  namespace App;

  use Illuminate\Database\Eloquent\Model;

  class TaxonomyTerm extends Model
  {
    protected $fillable = [
      'tid',
      'name', // English name
      'slug', // Same as the English name in snake case or -
      'en_title',
      'fr_title',
      'nl_title',
      'en_description',
      'fr_description',
      'nl_description',
      'en_alias',
      'fr_alias',
      'nl_alias',
      'en_search_alias',
      'fr_search_alias',
      'nl_search_alias',
      'image',
      'weight',
      'taxonomy_vocabulary_id',
      'parent_id'
    ];

    public function Vocabulary(){
     return $this->belongsTo('App\TaxonomyVocabulory');
    }
    /*
     * Taxonomy terms child parent mapping
     */
    public function Parent(){
      return $this->belongsTo('App\TaxonomyTerm');
    }

  }
