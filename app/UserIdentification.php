<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIdentification extends Model
{
  protected $fillable = [
    'uuid',
    'os_type_id',
    'status',
    'user_id'

  ];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */

  public function os_type(){
    return $this->belongsTo('App\OsType');
  }

  public function queue(){
    return $this->hasOne('App\MessageQueue');
  }

  public function user(){
    return $this->belongsTo('App\User');
  }
}
