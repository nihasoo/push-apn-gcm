<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageSize extends Model {
  protected $fillable = ['name', 'slug', 'height', 'width', 'type'];
}



