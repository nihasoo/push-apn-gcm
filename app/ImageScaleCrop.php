<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageScaleCrop extends Model
{
  protected $fillable = ['record_id', 'package'];
}
