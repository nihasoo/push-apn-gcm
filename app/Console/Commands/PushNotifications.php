<?php

namespace App\Console\Commands;

use App\Http\Controllers\GCMController;
use Illuminate\Console\Command;
use App\Http\Controllers\APNController;

class PushNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:push';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push Notifications command for APN and GCM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $APN  = new GCMController();
        $APN->pushNotifications();
    }
}
