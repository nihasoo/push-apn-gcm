<?php

namespace App\Http\Controllers;

use App\ImageScaleCrop;
use App\ImageSize;
use Illuminate\Http\Request;
use File;

use App\Http\Requests;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;
use App\Facades\Push;
use App\Facades\ImageActions;

class ImageScaleCropController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // import the Intervention Image Manager Class


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      echo  Push::test();

      $model = new ImageScaleCrop();

        return view('image_scale_crop.create',compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
              //retrieve scale and crop model
              $input = ['record_id'=>1,'package'=>'Taxonomy'];
              $model_scalecrop = new ImageScaleCrop($input);
              // saving the model
              if($model_scalecrop->save()) {
                return ImageActions::imageProcess($request);
              }

      return false;



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

  /**
   * @param \Illuminate\Http\Request $request
   *
   * @return mixed -resized cropped image binary output ( response array)
   *
   */
    public function preview(Request $request){
      // uploaded base image
      $file=  $request->get('data');
      // cropped cordinates
      $cordinates = $request->get('cordinates');
      //return resized cropped images binary outputs
        return (ImageActions::Preview($file,$cordinates));

    }
}
