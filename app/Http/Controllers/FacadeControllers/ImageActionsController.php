<?php

  namespace App\Http\Controllers\FacadeControllers;

  use App\ImageSize;
  use Illuminate\Http\Request;
  use App\Http\Controllers\Controller;
  use File;

  use App\Http\Requests;
  use Intervention\Image\Facades\Image;

  class ImageActionsController extends Controller {


    protected $image_sizes = [];

    public function __construct() {
      // image sizes model
      $this->image_sizes = ImageSize::all();
    }

    public function Preview($file, $cordinates) {
      // response array
      $response = [
        'status' => FALSE,
        'data' => []
      ];
      if ($this->image_sizes->count()) {
        $response['status'] = TRUE;
        foreach ($this->image_sizes As $image_size) {
          //cropped image object
          $img = '';
          // image cropping and resizing
          $img = $this->ResizeCrop($file, $cordinates, $image_size);
          if ($img) {
            //setting the response
            $response['data'][$image_size->name] = [
              'size_id' => $image_size->id,
              // return the binary output - different sizes of cropped image
              'binary_image' => $img->encode('data-url')
            ];
          }
        }
      }

      return $response;
    }

    protected function ResizeCrop($file, $cordinates, $image_size) {

      // validating parameters
      if (isset($file) &&
        isset($cordinates['w']) && //checking for cordinates
        isset($cordinates['h']) &&
        isset($cordinates['x']) &&
        isset($cordinates['y']) &&
        $cordinates['w'] && //if cordinates have values
        $cordinates['h'] &&
        $cordinates['x'] &&
        $cordinates['y'] &&
        isset($image_size) // validating the image size
      ) {
        return Image::make($file)
          //cropping
          ->crop($cordinates['w'], $cordinates['h'], $cordinates['x'], $cordinates['y'])
          // resizing
          ->fit((int) $image_size->width, (int) $image_size->height, function ($constraint) {
            $constraint->upsize();
          }, 'left');
      }
      return FALSE;
    }

    public function imageProcess(Request $request) {

      // response array
      $response = [
        'status' => FALSE,
        'messages' => []
      ];
      // making the image directory
      if ($request->hasFile('image') && $request->has('sizes') && $request->has('cordinates')) {
        // base folder name
        $base_folder_name = storage_path() . '/upload/image_' . rand(0, 1000) . time();
        $base_folder_create_status = File::makeDirectory($base_folder_name, 0777, TRUE);
        // if base image folder created
        if ($base_folder_create_status) {
          $image_file = $request->file('image');
          // base file name
          $base_file_name = $request->file('image')->getClientOriginalName();
          //saving base image
          $base_img = Image::make($image_file)
            ->save(($base_folder_name . '/' . $base_file_name));
          if ($base_img) {
            $response['status'] = TRUE;
            $response['messages'][] = "Base image has been saved successfully - base image location- " . $base_folder_name . '/' . $base_file_name . '<br/>';
            // making a directory for resized images
            $resized_folder_create_status = File::makeDirectory($base_folder_name . '/resized_images', 0777, TRUE);
            if ($resized_folder_create_status) {
              foreach ($request->get('sizes') As $image_id) {
                $img = NULL;
                $image_size = ImageSize::findorFail($image_id);
                if (isset($image_size)) {
                  // image cropping and resizing
                  $img = $this->ResizeCrop($image_file, $request->get('cordinates'), $image_size);
                  $result = $img->save($base_folder_name . '/resized_images/' . $image_size->name . '_' . $base_file_name);
                  if ($result) {
                    $response['messages'][] = "Resized image has been saved successfully - resized image location- " . $base_folder_name . '/resized_images/' . $image_size->name . '_' . $base_file_name . '<br/>';

                  }
                }
              }
            }
          }
        }
      }
      return $response;
    }

  }