<?php

  namespace App\Http\Controllers\FacadeControllers;

  use App\Http\Controllers\Auth\AuthController;
  use App\Http\Controllers\Auth\PasswordController;
  use App\Http\Middleware\Notifications;
  use App\Http\Controllers\Controller;
  use App\MessageQueue;
  use App\OsType;
  use App\User;
  use App\UserIdentification;
  use Illuminate\Http\Request;

  use App\Http\Requests;

  use Illuminate\Support\Facades\Auth;
  use Sly\NotificationPusher\PushManager,
    Sly\NotificationPusher\Adapter\Gcm as GcmAdapter,
    Sly\NotificationPusher\Adapter\Apns as ApnsAdapter,
    Sly\NotificationPusher\Collection\DeviceCollection,
    Sly\NotificationPusher\Model\Device,
    Sly\NotificationPusher\Model\Message,
    Sly\NotificationPusher\Model\Push;

  class PushNotificationController extends Controller {

    protected $model;
    protected $adapter_method;

    public function __construct() {

    }

    public function addNotifications(Request $request) {
      //check for the os type
      if ($request->has('os_type')) {
        $os_types = OsType::where('name', '=', $request->get('os_type'))->get();
        // if the requested os type exists
        if (count($os_types)) {
          $input = $request->all();
          // unset os type from the request in order to bind the relation later
          unset($input['os_type']);
          // retriving the OS type model
          $os_type_model = OsType::find($os_types[0]->id);
          // if the model retrieved....
          if ($os_type_model) {
            // declaring ststus of the uuid
            $input['status'] = 0;
            // user identfication model
            $uuid_model = new UserIdentification($input);
            // saving the uuid
            $uuid_model->save();
            // building the ostype relation
            $os_type_model->user_identification()->save($uuid_model);
            // if the request has user information
            if ($request->has('UID')) {
              // loading the auth controller class
              $user_controller = new AuthController();
              // validating the user information for the registration
              $is_registered = $user_controller->validate($request->get('UID'));
              // if the user already registered or valid
              if ($is_registered) {
                // loading the user model
                $user_model = User::find($is_registered->id);
                // building the relation between user and uuid
                $user_model->user_idenficiation->save($uuid_model);
              }
            }
            // unset uuid
            unset($input['uuid']);
            // load message queue model
            $queue_model = new MessageQueue($input);
            // saving reaquested queued data
            $queue_model->save();
            // building the relation between queue data and uuid
            $uuid_model->queue()->save($queue_model);
          }
        }
      }
    }

    protected function pushNotifications() {
      // get validated  queued adapter messages
      $adapter_messages = $this->getMessages();
      // if messages queue exists
      if (count($adapter_messages)) {
        // set the push manager environment - Default - sand box
        $pushManager = new PushManager(PushManager::ENVIRONMENT_DEV);
        foreach ($adapter_messages As $adapter => $devices_info) {
          // declare the adapter class
          $adapter_class = new $adapter . 'Adapter';
          // get adapter configurations
          $config_adapter_settings = config('app.adapter_configurations');
          // definning the  adapter class
          $Adapter = $adapter_class($config_adapter_settings[$adapter]);
          // initialising the device array
          $device_array = [];
          foreach ($devices_info As $device) {
            $device_array[] = new Device($device['device']);
          }
          // if devices are exists
          if (count($device_array)) {
            // Set the device(s) to push the notification to.
            $devices = new DeviceCollection($device_array);
            // Then, create the push skel.
            $message = new Message('This is a basic example of push.');
            // Finally, create and add the push to the manager, and push it!
            $push = new Push($Adapter, $devices, $message);
            $pushManager->add($push);
            $pushManager->push();
          }
        }
      }
    }


    protected function getMessages() {
      // initializing adapter messages
      $adapter_messages = [];
      // get all queued messages
      $messages_queue = MessageQueue::all();
      // get all OS types
      $ostypes = OsType::all();
      // if queued messages exists
      if (count($messages_queue)) {
        foreach ($messages_queue As $message) {
          // if OS typea exists
          if (count($ostypes)) {
            foreach ($ostypes as $ostype) {
              // check whether the message has an uuid
              if ($message->user_identification) {
                // load the uuid model
                $uuid_model = $message->user_identification;
                // if uuid related with an user
                if ($uuid_model->user) {
                  // if the user is a valid user
                  if ($uuid_model->user->status && $uuid_model->status == 0) {
                    // creating the adapter messages array grouped by adapter type
                    if ($ostype->name == $uuid_model->os_type->name) {
                      $adapter_messages[$ostype->adapter][] = [
                        'device' => $uuid_model->uuid,
                        'message' => $message->message,
                      ];
                    }
                  }
                }
              }
            }
          }
        }

      }
      // return the array
      return $adapter_messages;
    }

    public function test(){
      return "something";
    }
  }