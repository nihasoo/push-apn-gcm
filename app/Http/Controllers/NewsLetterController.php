<?php

namespace App\Http\Controllers;

use Response;
use App\NewsLetter;
use Illuminate\Http\Request;

use App\Http\Requests;
use Mockery\Tests\React_ReadableStreamInterface;

class NewsLetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // news letter model
       $model  = NewsLetter::all();
        return view('news_letter.index' , compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // ranges for date month and year propagating to the blade view
        $dates  = range(1,31);
        $months = range(1,12);
        $years  = range(1950, date('Y'));
        return  view('news_letter.create', compact('dates','months','years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function add(Request $request){

        $response=[
            'data' => [],
            'status'=> FALSE
        ];
        // input for the model
        $input = $request->get('data');
        // newsletter model
        $news_letter  = new NewsLetter($input);
        $news_letter->save();
        if($news_letter) {
            $response['status'] = TRUE;
        }
        return $response;

    }

    public function exportCSV(){

        // newslettter model
        $model = NewsLetter::all();
        // if newslatters are exist
        if(count($model)) {
            // file name of the file to be export
            $filename = "news_letter.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('email', 'birthday', 'postal_code'));

            foreach ($model as $row) {
                fputcsv($handle, array(
                  $row['email'],
                  $row['birthday'],
                  $row['postal_code']
                ));
            }

            fclose($handle);
            $headers = array(
              'Content-Type' => 'text/csv',
            );

            // return the response
            return Response::download($filename, 'news_letter.csv', $headers);
        }
    }
}
