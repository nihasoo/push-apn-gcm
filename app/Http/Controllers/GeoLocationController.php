<?php

  namespace App\Http\Controllers;

  use Illuminate\Http\Request;

  use App\Http\Requests;

  class GeoLocationController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
//        return view('geo_location.map');
//        return view('geo_location.index');
      return view('geo_location.drag');
      return view('geo_location.multiple_output');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
      //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
      //
    }

    public function getAddresses() {
      $response = [
        'status' => FALSE,
        'addresses' => []
      ];
      $addresses = [
        1 => "Boulevard Lambermont, 1030 Schaerbeek, Belgique",
//        2 => "Boulevard Auguste Reyers 163, 1030 Schaerbeek, Belgique",
//        3 => "Avenue Henry Dunant 11, 1140 Evere, Belgique",
//        4 => "Rue de l'Hôpital 7, 1000 Bruxelles, Belgique"
      ];
      $response['status'] = true;
      $response['addresses'] = $addresses;

      return $response;
    }
  }
