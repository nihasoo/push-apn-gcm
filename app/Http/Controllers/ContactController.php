<?php

  namespace App\Http\Controllers;

  use App\Contact;
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Mail;


  use App\Http\Requests;

  class ContactController extends Controller {
    public function create() {


      return view('contact.create');
    }

    public function add(Request $request) {

      // initializing admin and customer email sent status
      $admin_mail_status = FALSE;
      $customer_mail_status = FALSE;
      // response array
      $response = ['email_status' => FALSE, 'status' => FALSE, 'data' => []];
      // input data
      $input = $request->get('data');
      if ($input) {
        // contact model
        $contact_model = new Contact($input);
        // save model
        $contact_model->save();
        // if the contact has been inserted successfully...
        if ($contact_model) {
          // status of the response is being set to true
          $response['status'] = TRUE;
          // send email - admin
          $admin_mail_status = Mail::send('emails.welcome', array(), function ($message) use ($input) {
            $message
              //admin email
              ->to('nihsoo@gmail.com')
              // admin email
              ->from('nihasoo@gmail.com')
              ->subject('TEST');
          });
          // send email - customer

          $customer_mail_status = Mail::send('emails.welcome', array(), function ($message) use ($input) {
            $message
              // customer email
              ->to($input['email'])
              // admin email
              ->from('nihasoo@gmail.com')
              ->subject('TEST');
          });

          // if both admin and customer emails have  been sent succesfully
          if ($customer_mail_status && $admin_mail_status) {
            // set email sent staus to true
            $response['email_status'] = TRUE;
          }
        }


      }
      // returns the response
      return $response;

    }
  }
