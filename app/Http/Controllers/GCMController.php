<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Apple\ApnPush\Notification;
use Apple\ApnPush\Certificate\Certificate;
use Apple\ApnPush\Notification\Connection;
use Apple\ApnPush\Queue;
use Apple\ApnPush\Queue\Adapter\ArrayAdapter;

use Sly\NotificationPusher\PushManager,
  Sly\NotificationPusher\Adapter\Gcm as GcmAdapter,
  Sly\NotificationPusher\Collection\DeviceCollection,
  Sly\NotificationPusher\Model\Device,
  Sly\NotificationPusher\Model\Message,
  Sly\NotificationPusher\Model\Push;

use App\Http\Requests;

class GCMController extends Controller
{
    public function __construct() {

    }

  public function pushNotifications(){
// API access key from Google API's Console

// prep the bundle
    $pushManager = new PushManager(PushManager::ENVIRONMENT_DEV);
//
//    $config_adapter_settings  = config('app.adapter_configurations');
//    dd($config_adapter_settings['Apns']);
// Then declare an adapter.
    $gcmAdapter = new GcmAdapter(array(
      'apiKey' => 'AIzaSyCTzrMQSTHFbcpooVBQCbHzwYEhqjP-8-8',
    ));

// Set the device(s) to push the notification to.
    $devices = new DeviceCollection(array(
      new Device('cJiBRt7srYs:APA91bHvabm2sqaERaHkbi9_CBeBLiUyal0ggRoJ8h3YdwqpSnd6REqJpVvBlgqZNCt7jbXUqtz_1yK1H8gp1Lv4u6mg6q79WtJbTjc-4yvWBpWshtg180PQJpynKsGWWpVbQ_OKm9eh'),

    ));


// Then, create the push skel.
    $message = new Message('This is an example - gryrghghghdghdghdgd.');

// Finally, create and add the push to the manager, and push it!
    $push = new Push($gcmAdapter, $devices, $message);

    $pushManager->add($push);
    $pushManager->push();
  }

  public function apnsPushNotifications(){

      $pushManager = new PushManager(PushManager::ENVIRONMENT_DEV);

// Then declare an adapter.
      $apnsAdapter = new ApnsAdapter(array(
        'certificate' => base_path() . '/resources/certificates/push_cert_dist.pem',
      ));

// Set the device(s) to push the notification to.
      $devices = new DeviceCollection(array(
        new Device('323d15031b6294b65489df48114ac1a0ed8a42f80ef21c371190c35b496a4e80'),
        // ...
      ));

// Then, create the push skel.
      $message = new Message('This is a basic example of push.');

// Finally, create and add the push to the manager, and push it!
      $push = new Push($apnsAdapter, $devices, $message);

      $pushManager->add($push);
      $pushManager->push();

  }
}
