<?php

namespace App\Http\Middleware;

use Closure;

class Notifications
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->input('os-type')){
            return response()->json([
              'Response' => 404,
              'status' => FALSE,
              'message' => 'OS type found'
            ])
              ->setCallback($request->input('callback'));
            //return abort(404, 'OS type not found');
        }
        if(!$request->input('token')){
            return response()->json([
              'Response' => 404,
              'status' => FALSE,
              'message' => 'Token not found'
            ])
              ->setCallback($request->input('callback'));
            //return abort(404, 'Token not found');
        }
        return $next($request);
    }
}
