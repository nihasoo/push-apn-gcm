<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $img = Image::make(public_path().'/foo.jpg')->fit(200, 200, function ($constraint) {
        $constraint->upsize();
    },'left');
//    dd($img);

    return $img->response('jpg');
});
//
//    Route::get('/apn-notifications', [
////      'middleware' => ['notifications'],
//      'as' => 'apn-push-notifications.push',
//      'uses' => 'PushNotificationController@pushNotifications'
//    ]);

    Route::get('/add-notifications', [
//      'middleware' => ['notifications'],
      'as' => 'push-notifications.add',
      'uses' => 'FacadeControllers\PushNotificationController@addNotifications'
    ]);
    Route::get('/gcm-notifications', [
//      'middleware' =`
      'as' => 'gcm-push-notifications.push',
      'uses' => 'GCMController@pushNotifications'
    ]);

    Route::get('auth/{app}', 'Auth\AuthController@redirectToProvider');
    Route::get('auth/{app}/callback', 'Auth\AuthController@handleProviderCallback');

    #region image actions
    Route::post('image-scale-crop/preview', [
      'as' => 'image-scale-crop.preview',
      'uses' => 'ImageScaleCropController@preview'
    ]);

    Route::post('image-scale-crop/crop', [
      'as' => 'image-scale-crop.crop',
      'uses' => 'ImageScaleCropController@crop'
    ]);

    Route::get('image-size/seeder', [
      'as' => 'image-size.seeder',
      'uses' => 'ImageSizeController@seeder'
    ]);
    #endregion
    # region newsletter and contact

    Route::post('news-letter/add', [
      'as' => 'news-letter.add',
      'uses' => 'NewsLetterController@add'
    ]);
    Route::get('news-letter/export-csv', [
      'as' => 'news-letter.export-csv',
      'uses' => 'NewsLetterController@exportCSV'
    ]);
    Route::get('contact/create', [
      'as' => 'contact.create',
      'uses' => 'ContactController@create'
    ]);
    Route::post('contact/add', [
      'as' => 'contact.add',
      'uses' => 'ContactController@add'
    ]);
    #endregion

    #region geo loaction
    Route::get('geo-location/addresses', [
      'as' => 'geo-location.addresses',
      'uses' => 'GeoLocationController@getAddresses'
    ]);
    #endregion

    #region resource controllers
    Route::resource('image-scale-crop', 'ImageScaleCropController');
    Route::resource('image-size', 'ImageSizeController');
    Route::resource('geo-location', 'GeoLocationController');
    Route::resource('news-letter', 'NewsLetterController');
    #endregion

#region vuetests
    Route::resource('vue','VueController');
#endregion
