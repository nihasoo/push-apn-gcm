<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsType extends Model
{
  protected $fillable = [
    'name',
  ];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */

  public function user_identification(){
    return $this->hasMany('App\UserIdentification');
  }
}
