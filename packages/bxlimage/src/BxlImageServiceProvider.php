<?php

  namespace bxlimage;

  use Illuminate\Support\ServiceProvider;

  class BxlImageServiceProvider extends ServiceProvider
  {

    public function register() {
      $this->app->bind('bccxlimage' , function($app){
      return new BxlImage;
      });
    }

    public function boot(){
      //loading the route files
      require __DIR__ . '/BxlImage/Http/routes.php';

      // define views of the package

      $this->loadViewsFrom(__DIR__ . '/resources','bxlimage');
    }
  }