<?php
  namespace bxlimage\Http\Controllers;



  use App\Http\Controllers\Controller;

  class ImageSizeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
//      $image_sizes = ImageSize::all();
//      return view('bxlimage::image_size.index', compact('image_sizes'));
      return view('bxlimage::image_size.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
      $model = new ImageSize();
      return view('image_size.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $this->validate($request, [
        'name' => 'required'
      ]);

      if (isset($request)) {
        $input = $request->all();
        // check for requsted parameters
        if (count($input)) {
          $result = ImageSize::create($input);
          // if the insert is success
          if ($result) {
            return redirect('image-size');
          }
          else {
            return redirect()->back()->withInput();
          }


        }
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
      if (is_numeric($id) && //validate for the record id
        !empty($id) &&
        $id != 0
      ) {
        $model = ImageSize::findOrFail($id);
        if (isset($model)) {
          return view('image_size.edit', compact('model'));

        }
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      if (is_numeric($id) && //validate for the record id
        !empty($id) &&
        $id != 0
      ) {
        $input = $request->all();
        $update_status = ImageSize::find($id)->update($input);
        //redirects if the record is updated
        if ($update_status) {
          return redirect('image-size');
        }
        else {
          return redirect()->back()->withInput();
        }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
      if (is_numeric($id) && //validate for the record id
        !empty($id) &&
        $id != 0
      ) {
        $model = ImageSize::findOrFail($id);
        $delete_status = $model->delete();
        //redirects if successfully deleted
        if ($delete_status) {
          return redirect('image-size');
        }
        else {
          return redirect()->back()->withInput();
        }
      }
    }
  }