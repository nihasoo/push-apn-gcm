<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('Name', NULL, ['class' => 'col-sm-5 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::text('name', NULL ,['class' => 'form-control'])  !!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('Slug', NULL, ['class' => 'col-sm-5 control-label ']) !!}
            <div class="col-sm-5">
                {!! Form::text('slug', NULL ,['class' => 'form-control'])  !!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group" id="Assigned_contacts_form_group">
            {!! Form::label('Width', NULL, ['class' => 'col-sm-5 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::text('width', NULL ,['class' => 'form-control'])  !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('Height', NULL, ['class' => 'col-sm-5 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::text('height', NULL ,['class' => 'form-control'])  !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('Priority', NULL, ['class' => 'col-sm-5 control-label ']) !!}
            <div class="col-sm-5">
                {!! Form::select('type', [
                  'Article'=>'Article',
                  'Address'=>'Address',
                  'Taxonomy'=>'Taxonomy',
                  'Agenda'=>'Agenda',
                  'BBN'=>'BBN',
                  'Folder'=>'Folder',
                  'User'=>'User',], NULL ,['class' => 'form-control chosen-select']) !!}
            </div>
        </div>
    </div>

</div>
