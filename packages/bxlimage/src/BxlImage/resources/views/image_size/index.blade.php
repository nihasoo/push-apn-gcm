<table class="table table-striped table-bordered table-hover " id="editable" >
    <thead>
    <tr>
        <th>Name </th>
        <th>Description </th>
        <th>Created Date</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($action_types as $action_type)
        <tr>

            <td>{{$action_type->name}}</td>
            <td>{{$action_type->size}}</td>
            <td>{{$action_type->created_at}}</td>
            <td><a href="{{ route('crm.action-type.edit' , $action_type->id) }}"><i class="fa fa-check text-navy"></i></a>
                <a href="{{ route('crm.action-type.destroy' , $action_type->id) }}" class="btn btn-danger action_confirm" data-method="delete" data-token="{{ csrf_token() }}"><i class="glyphicon glyphicon-remove"></i></a></td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Name </th>
        <th>Description </th>
        <th>Created Date</th>
        <th>Action</th>
    </tr>
    </tfoot>
</table>