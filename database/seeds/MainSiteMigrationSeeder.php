<?php

  use Illuminate\Database\Seeder;
  use App\TaxonomyTerm;
  use App\TaxonomyVocabulary;

  class MainSiteMigrationSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {


      $vocabulary   = $this->addVocabulory();
      $taxonomies = $this->getTaxonomies();
      $taxonomy_terms = [];
      if ($taxonomies) {
        foreach ($taxonomies As $taxonomy) {

          $aliases = DB::connection('bxl_drupal')
            ->select("SELECT td.tid,
                                   td.name as name,
                                   td.name as slug,

                                   ua_fr.field_c4_section_url_alias_value as title_fr,
                                   ua_en.field_c4_section_url_alias_value as title_en,
                                   ua_nl.field_c4_section_url_alias_value as title_nl,

                                   td.description as description_fr ,
								   td.description as description_en ,
								   td.description as description_nl ,

                                   ul_fr.alias as alias_fr,
                                   ul_en.alias as alias_en,
                                   ul_nl.alias as alias_nl,

                                   td.name as search_alias_en,
                                   th.parent  as parent

                            FROM brusselslife_taxonomy_term_data td
                            INNER JOIN brusselslife_taxonomy_term_hierarchy th on(th.tid = td.tid)
                            INNER JOIN (SELECT * from brusselslife_field_data_field_c4_section_url_alias ua where ua.delta = 0) ua_fr ON(ua_fr.entity_id = td.tid)
                            INNER JOIN (SELECT * from brusselslife_field_data_field_c4_section_url_alias ua where ua.delta = 1) ua_en ON(ua_en.entity_id = td.tid)
                            INNER JOIN (SELECT * from brusselslife_field_data_field_c4_section_url_alias ua where ua.delta = 2) ua_nl ON(ua_nl.entity_id = td.tid)

                            left JOIN (SELECT ul.pid, REPLACE(ul.source, 'taxonomy/term/', '' ) as tid ,ul.alias  from brusselslife_url_alias ul where ul.language = 'fr' AND  ul.source LIKE '%taxonomy/term/%') ul_fr ON(ul_fr.tid = td.tid)
                            left JOIN (SELECT ul.pid, REPLACE(ul.source, 'taxonomy/term/', '' ) as tid ,ul.alias  from brusselslife_url_alias ul where ul.language = 'en' AND  ul.source LIKE '%taxonomy/term/%') ul_en ON(ul_en.tid = td.tid)
                            left JOIN (SELECT ul.pid, REPLACE(ul.source, 'taxonomy/term/', '' ) as tid ,ul.alias  from brusselslife_url_alias ul where ul.language = 'nl' AND  ul.source LIKE '%taxonomy/term/%') ul_nl ON(ul_nl.tid = td.tid)

                            WHERE td.tid = " . $taxonomy->tid . "
                           ");
          // returns taxonomy terms

          $terms = $this->taxonomyTranslate($taxonomy);
          $array_aliases  = (array) $aliases[0];
          $parent_taxonomy  = $array_aliases['parent'];
          unset($array_aliases['parent']);
          $taxonomy_terms = array_merge($array_aliases, (array) $terms[0]);
          //retrieving taxonomy terms model
          $taxonomy_model = new TaxonomyTerm($taxonomy_terms);
          $taxonomy_model->save();
          $vocabulary_term = TaxonomyVocabulary::where('vid' ,'=',$taxonomy->vid)->get();
          if(count($vocabulary_term)){
            $vocabularymodel = TaxonomyVocabulary::find($vocabulary_term[0]->id);
            $vocabularymodel->Taxonomy()->save($taxonomy_model);
            $parent_taxonomy_model  = TaxonomyTerm::find($parent_taxonomy);
            if(isset($parent_taxonomy_model)){
              $parent_taxonomy_model->Parent()->save($taxonomy_model);
            }

          }
        }
      }

      return TRUE;

    }

    /**
     * Translate taxonomy term list
     *
     * @param array $terms Term list
     *
     * @return array translated term list
     */
    private function taxonomyTranslate($terms) {

      if (count($terms)) {
        // Transform term query result to an array of string ready for the language query
        $str = 'term:' . $terms->tid . ':name';
        $terms = DB::connection('bxl_drupal')
          ->select("SELECT t_fr.translation as search_alias_fr,

                           t_nl.translation as search_alias_nl
                    FROM brusselslife_i18n_string s
                    LEFT JOIN (SELECT * FROM brusselslife_locales_target t WHERE t.language = 'fr') t_fr on t_fr.lid=s.lid
                    LEFT JOIN (SELECT * FROM brusselslife_locales_target t WHERE t.language = 'nl') t_nl on t_nl.lid=s.lid
                    WHERE  s.context ='" . $str . "'");
        if (count($terms)) {
          return $terms;
        }
        return FALSE;
      }
      return FALSE;
    }

    /**
     * get Taxonomy list
     *
     * @return array $taxonomies list
     *
     */
    private function getTaxonomies() {
      $taxonomies = DB::connection('bxl_drupal')
        ->select("SELECT td.tid , td.vid, td.name,td.description from brusselslife_taxonomy_term_data td WHERE td.vid = 10 limit 0,10 ");
      if (count($taxonomies)) {
        return $taxonomies;
      }
      return FALSE;
    }

    public function addVocabulory() {
      $vocabulary = DB::connection('bxl_drupal')
                       ->select("SELECT
                        tv.vid ,
                       tv_en.name as en_title,
                       tv_fr.name as fr_title,
                       tv_nl.name as nl_title,
                       tv_en.description as en_description,
                       tv_fr.description as fr_description,
                       tv_nl.description as nl_description
                      FROM brusselslife_taxonomy_vocabulary tv
                      LEFT JOIN (SELECT * FROM brusselslife_taxonomy_vocabulary tv WHERE tv.language = 'fr') tv_fr on tv_fr.vid=tv.vid
                      LEFT JOIN (SELECT * FROM brusselslife_taxonomy_vocabulary tv WHERE tv.language = 'en') tv_en on tv_en.vid=tv.vid
                      LEFT JOIN (SELECT * FROM brusselslife_taxonomy_vocabulary tv WHERE tv.language = 'nl') tv_nl on tv_nl.vid=tv.vid


      ");
//dd($vocabulary);

      if(count($vocabulary)){
        foreach($vocabulary As $term){
//          dd($term);
          $vocabulary_model  = new TaxonomyVocabulary((array)$term);
          $vocabulary_model->save();
        }

        return true;
      }
      return false;
    }


  }

