<?php

  use Illuminate\Database\Seeder;

  class MainSiteMigrationSeedersss extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      $taxonomies = $this->getTaxonomies();
      $taxonomy_terms = [];

      if ($taxonomies) {
        foreach ($taxonomies As $taxonomy) {
          foreach (['fr', 'en', 'nl'] as $index => $language) {
            // returns taxonomy terms
            $taxonomy_name = $this->taxonomyTranslate($taxonomy, $language);
            // if the taxonomy is translatable
            if (count($taxonomy_name ) &&  $language != 'en') {
              $taxonomy_terms[$taxonomy->tid][$language]['name'] = $taxonomy_name[0]->name_lang;
            }else{
              $taxonomy_terms[$taxonomy->tid][$language]['name'] = $taxonomy->name;
            }
            $taxonomy_terms[$taxonomy->tid][$language]['description'] = $taxonomy->description;

            // retrieving aliases
            $alias = DB::connection('bxl_drupal')
              ->select("SELECT
                                   ua_lang.field_c4_section_url_alias_value as alias,
                                   ul_lang.alias as search_alias
                            FROM (SELECT * from brusselslife_field_data_field_c4_section_url_alias ua where ua.delta = " . $index . ") ua_lang
                            LEFT JOIN (SELECT ul.pid, REPLACE(ul.source, 'taxonomy/term/', '' ) as tid ,ul.alias  from brusselslife_url_alias ul where ul.language = '" . $language . "' AND  ul.source LIKE '%taxonomy/term/%') ul_lang ON(ul_lang.tid = ua_lang.entity_id)
                            WHERE ua_lang.entity_id = " . $taxonomy->tid . "
                           ");
            $taxonomy_terms[$taxonomy->tid][$language]['alias'] = $alias[0]->alias;
            $taxonomy_terms[$taxonomy->tid][$language]['search_alias'] = $alias[0]->search_alias;
          }
        }
      }
      dd($taxonomy_terms);
      return $taxonomy_terms;

    }

    /**
     * Translate taxonomy term list
     *
     * @param array $terms Term list
     *
     * @return array translated term list
     */
    private function taxonomyTranslate($terms, $language) {

      if (count($terms) && $language) {
        // Transform term query result to an array of string ready for the language query
        $str = 'term:' . $terms->tid . ':name';
        $term = DB::connection('bxl_drupal')
          ->select("SELECT t_lang.translation as name_lang
                    FROM brusselslife_i18n_string s
                    LEFT JOIN (SELECT * FROM brusselslife_locales_target t WHERE t.language = '" . $language . "') t_lang on t_lang.lid=s.lid
                    WHERE  s.context ='" . $str . "'");

        if (count($term)) {
          return $term;
        }
        return FALSE;
      }
    }

    /**
     * get Taxonomy list
     *
     * @return array $taxonomies list
     *
     */
    private function getTaxonomies() {
      $taxonomies = DB::connection('bxl_drupal')
        ->select("SELECT td.tid , td.name,td.description from brusselslife_taxonomy_term_data td WHERE td.vid = 10 limit 0,10 ");
      if (count($taxonomies)) {
        return $taxonomies;
      }
      return FALSE;
    }

  }
