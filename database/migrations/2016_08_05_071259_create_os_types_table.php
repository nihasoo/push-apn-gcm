<?php

  use Illuminate\Database\Schema\Blueprint;
  use Illuminate\Database\Migrations\Migration;

  class CreateOsTypesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      Schema::create('os_types', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name')->nullable()->comment = "name of the OS type";
        $table->string('adapter_type')
          ->nullable()->comment = "Adapter type of the OS";
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      Schema::drop('os_types');
    }
  }
