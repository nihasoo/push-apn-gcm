<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageScaleCropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_scale_crops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('record_id')->comment  = "record ID of the package";
            $table->text('package')->comment  = "name of the package";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('image_scale_crops');
    }
}
