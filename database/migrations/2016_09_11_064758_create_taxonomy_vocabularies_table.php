<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxonomyVocabulariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxonomy_vocabularies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vid')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('en_title')->nullable();
            $table->string('fr_title')->nullable();
            $table->string('nl_title')->nullable();
            $table->text('en_description')->nullable();
            $table->text('fr_description')->nullable();
            $table->text('nl_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('taxonomy_vocabularies');
    }
}
