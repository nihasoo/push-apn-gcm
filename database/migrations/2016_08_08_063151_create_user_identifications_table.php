<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserIdentificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_identifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('os_type_id')
              ->unsigned()
              ->nullable()->comment = "OS type ID";
            $table->foreign('os_type_id')
              ->references('id')
              ->on('os_types');
            $table->enum('status', [
              '0',
              '1',
            ])->nullable()->default('0')->comment = "Status of uuid";
            $table->integer('user_id')
              ->unsigned()
              ->nullable()->comment = "User ID";
            $table->foreign('user_id')
              ->references('id')
              ->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_identifications');
    }
}
