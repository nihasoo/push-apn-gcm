<?php

  use Illuminate\Database\Schema\Blueprint;
  use Illuminate\Database\Migrations\Migration;

  class CreateMessageQueuesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      Schema::create('message_queues', function (Blueprint $table) {
        $table->increments('id');
        $table->string('message')->nullable;
        $table->integer('user_identification_id')
          ->unsigned()
          ->nullable()->comment = "uuid";
        $table->foreign('user_identification_id')
          ->references('id')
          ->on('user_identifications');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      Schema::drop('message_queues');
    }
  }
