<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxonomyTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxonomy_terms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tid')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('en_title');
            $table->string('fr_title');
            $table->string('nl_title');
            $table->text('en_description')->nullable();
            $table->text('fr_description')->nullable();
            $table->text('nl_description')->nullable();
            $table->string('en_alias')->nullable();
            $table->string('fr_alias')->nullable();
            $table->string('nl_alias')->nullable();
            $table->string('en_search_alias')->nullable();
            $table->string('fr_search_alias')->nullable();
            $table->string('nl_search_alias')->nullable();
            $table->string('image')->nullable();
            $table->string('icon')->nullable();
            $table->string('weight')->nullable();
            $table->integer('taxonomy_vocabulary_id')->unsigned()->nullable();
            $table->foreign('taxonomy_vocabulary_id')
              ->references('id')
              ->on('taxonomy_vocabularies');
            $table->integer('parent_id')->unsigned()->nullable()->comment = "parent child mapping for taxonomies";
            $table->foreign('parent_id')
              ->references('id')
              ->on('taxonomy_terms');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('taxonomy_terms');
    }
}
